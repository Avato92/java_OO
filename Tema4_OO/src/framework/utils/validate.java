package framework.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class validate {
	private static final String template_name = "^([A-Z]{1})([a-z])*$";
	private static final String template_date = "(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))";

	public static boolean validate_name(String Nombre) {
		Pattern pattern = Pattern.compile(template_name);
		Matcher matcher = pattern.matcher(Nombre);
		if (matcher.matches()) {
			return true;
		} else
			return false;
	}

	public static boolean validate_date(String date) {
		Pattern pattern = Pattern.compile(template_date);
		Matcher matcher = pattern.matcher(date);
		if (matcher.matches()) {
			return true;
		} else
			return false;
	}
}
