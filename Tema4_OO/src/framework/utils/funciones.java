package framework.utils;

import javax.swing.JOptionPane;

public class funciones {

	///////////////////////////////////////////////////// Valida_Int/////////////////////////////////////////////////////////////////
	public static int valida_Int(String titulo, String message) {
		int num = 0;
		String a = "";
		boolean correcto = true;

		do {
			try {
				a = JOptionPane.showInputDialog(null, message, titulo, JOptionPane.QUESTION_MESSAGE);
				if (a == null) {
					JOptionPane.showMessageDialog(null, "Saliendo de la aplicaci�n", "Saliendo",
							JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				} else {
					num = Integer.parseInt(a);
					correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No has introducido un n�mero entero", "Error",
						JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return num;

	}

	//////////////////////////////////////// Valida_Int!=0////////////////////////////////////////////////////////////////////////////
	public static int validaIntdivision(String titulo, String message) {
		boolean correcto = true;
		int num2 = 0;
		String b = "";

		do {
			try {
				b = JOptionPane.showInputDialog(null, message, titulo, JOptionPane.QUESTION_MESSAGE);
				if (b == null) {
					JOptionPane.showMessageDialog(null, "No ha introducido ningun numero", "Atenci�n",
							JOptionPane.INFORMATION_MESSAGE);
					correcto = false;
				} else {
					num2 = Integer.parseInt(b);
					if (num2 == 0) {
						correcto = false;
						JOptionPane.showMessageDialog(null, "El numero divisor no puede ser cero", "Atenci�n",
								JOptionPane.INFORMATION_MESSAGE);
					} else
						correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No ha introducido un numero entero", "Error",
						JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return num2;
	}

	/////////////////////////////////////////////// Valida_float/////////////////////////////////////////////////////////////////////
	public static float valida_Float(String titulo, String message) {
		float f = 0.0f;
		boolean correcto = true;
		String a = "";

		do {
			try {
				a = JOptionPane.showInputDialog(null, message, titulo, JOptionPane.QUESTION_MESSAGE);
				if (a == null) {
					JOptionPane.showMessageDialog(null, "No ha introducido ningun numero", "Atenci�n",
							JOptionPane.INFORMATION_MESSAGE);
					correcto = false;
				} else {
					f = Float.parseFloat(a);
					correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No ha introducido un numero float", "Error",
						JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return f;
	}

	////////////////////////////////////////////// Valida_char////////////////////////////////////////////////////////////////////////////
	public static char valida_Char(String titulo, String message) {
		char c = 0;
		boolean correcto = true;
		String s = "";

		do {
			try {
				s = JOptionPane.showInputDialog(null, message, titulo, JOptionPane.QUESTION_MESSAGE);
				if (s == null) {
					JOptionPane.showMessageDialog(null, "No ha introducido ningun caracter", "Atenci�n",
							JOptionPane.INFORMATION_MESSAGE);
					correcto = false;
				} else {
					c = s.charAt(0);
					correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No ha introducido una letra", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return c;
	}

	////////////////////////////////////////////////// Valida_String/////////////////////////////////////////////////////////////////////////
	public static String validaString(String message, String titulo) {
		boolean correcto = true;
		String a = "";

		do {
			try {
				a = JOptionPane.showInputDialog(null, message, titulo, JOptionPane.QUESTION_MESSAGE);
				correcto = true;
				if (a == null) {
					JOptionPane.showMessageDialog(null, "No ha introducido ninguna cadena", "Atenci�n",
							JOptionPane.INFORMATION_MESSAGE);
					correcto = false;
				}
				if (a.equals("")) {
					JOptionPane.showMessageDialog(null, "Error de introducci�n de datos", "Error",
							JOptionPane.ERROR_MESSAGE);
					correcto = false;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No ha introducido una cadena", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return a;
	}

	///////////////////////////////////////////////////// OPCIONES_DE_MENU///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////// COMBO_MENU///////////////////////////////////////////////////////////////////////
	public static String combomenu(String message, String title, String[] opcions) {

		String op = "";
		Object selection = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE, null,
				opcions, 0);
		if (selection == null) {
			JOptionPane.showMessageDialog(null, "No hay datos guardados", "Error", JOptionPane.INFORMATION_MESSAGE);
			op = "";
		} else
			op = (String) selection;
		return op;
	}

	/////////////////////////////////////////////////////////// MENU_BOTONS/////////////////////////////////////////////////////////////////
	public static int menu_botones(String[] operaciones, String message, String titulo) {

		int op = 0;

		op = JOptionPane.showOptionDialog(null, message, titulo, 0, JOptionPane.QUESTION_MESSAGE, null, operaciones,
				operaciones[0]);

		return op;

	}

	///////////////////////////////////////////////////////// DESEA_CONTINUAR?///////////////////////////////////////////////////////////////
	public static int continuar(String[] operaciones) {
		int opcion = 0;
		opcion = JOptionPane.showOptionDialog(null, "�Que desea hacer?", "Men�", 0, JOptionPane.QUESTION_MESSAGE, null,
				operaciones, operaciones[0]);

		return opcion;

	}

	///////////////////////////////////////////////////// JOptionMessage///////////////////////////////////////////////////////////////////////////
	public static void print(String a) {
		JOptionPane.showMessageDialog(null, a);
	}
}