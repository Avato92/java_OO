package framework;

import java.util.ArrayList;

import framework.clases.Language;
import framework.moduls.classes.cold_drink;
import framework.moduls.classes.comida;
import framework.moduls.classes.hot_drink;
import framework.moduls.classes.singleton;
import framework.moduls.classes.dummies.dummies;
import framework.moduls.utils.funtions_class;
import framework.moduls.utils.funtions_login;
import framework.utils.funciones;

public class ppal {

	public static void main(String[] args) {
		int op = -1, op2 = -1, op3 = -1;
		String[] language = { "Castellano", "Valenci�", "English" };
		int op4 = funciones.menu_botones(language, "Seleccione idioma\nSelect a language", "Idioma");
		Language lang = new Language(op4);
		boolean continuar = true, continuar2 = true;
		String[] operaciones = lang.operationsConsumption(op4);
		String[] operaciones1 = lang.operationsCRUD(op4);
		String[] operaciones2 = lang.Y_N(op4);
		singleton.consumption_food = new ArrayList<comida>();
		singleton.consumption_cold_drink = new ArrayList<cold_drink>();
		singleton.consumption_hot_drink = new ArrayList<hot_drink>();
		singleton.consumptions = new ArrayList<String>();

		funtions_login.login();
		op3 = funciones.menu_botones(operaciones2, lang.messageWorkDummies(op4), "Dummies");
		if (op3 == 0) {
			dummies.work_dummies();
		}
		do {
			op = funciones.menu_botones(operaciones, lang.messageChoiceconsumption(op4),
					lang.messageChoiceconsumption(op4));
			if (op == 3) {
				continuar = false;
			} else {
				do {
					op2 = funciones.menu_botones(operaciones1, lang.messageCrud(op4), "CRUD");
					switch (op2) {
					case 0:
						funtions_class.create(op, op4);
						continuar2 = false;
						break;
					case 1:
						funtions_class.read(op);
						continuar2 = false;
						break;
					case 2:
						funtions_class.update(op, op4);
						continuar2 = false;
						break;
					case 3:
						funtions_class.delete(op);
						continuar2 = false;
						break;
					case 4:
						funtions_class.order(op);
						continuar2 = false;
						break;
					case 5:
						op4 = funciones.menu_botones(language, "Seleccione idioma\nSelect a language", "Idioma");
					case 6:
						continuar2 = false;
						break;
					default:
						continuar2 = false;
						continuar = false;
					}
				} while (continuar2 == true);

			}

		} while (continuar == true);
		funciones.print(lang.bye(op4));
	}
}
