package framework.moduls.classes;

import framework.clases.fecha;

public class hot_drink extends consumiciones {
	private int temperature;
	private String sugar;
	private String cafeine;

	public hot_drink(String nombre, String primary_key, fecha finicio, fecha fretirada, int media_cons, int temperature,
			String sugar, String cafeine) {
		super(nombre, primary_key, finicio, fretirada, media_cons);
		this.temperature = temperature;
		this.sugar = sugar;
		this.cafeine = cafeine;
	}

	public hot_drink(String primary_key) {
		super(primary_key);
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public void setSugar(String sugar) {
		this.sugar = sugar;
	}

	public void setCafeine(String cafeine) {
		this.cafeine = cafeine;
	}

	public int getTemperature() {
		return this.temperature;
	}

	public String getSugar() {
		return this.sugar;
	}

	public String getCafeine() {
		return this.cafeine;
	}

	public String toString() {
		return "Consumici�n " + getNombre() + "\nFecha de alta " + getFinicio() + "\nFecha de retirada "
				+ getFretirada() + "\nMedia al dia " + getMedia_cons() + "\nKarma " + getKarma()
				+ "\nLa temperatura de la bebieda caliente es " + temperature + "\nLo acompa�a con " + sugar + "\nEs "
				+ cafeine;
	}

}
