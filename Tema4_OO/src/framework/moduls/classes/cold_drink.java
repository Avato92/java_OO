package framework.moduls.classes;

import framework.clases.fecha;

public class cold_drink extends consumiciones {
	private int azucar;
	private int temperatura_media;
	private String health;
	private fecha promotion;

	public cold_drink(String nombre, String primary_key, fecha finicio, fecha fretirada, int media_cons, int azucar,
			int temperatura_media, fecha promotion) {
		super(nombre, primary_key, finicio, fretirada, media_cons);
		this.azucar = azucar;
		this.temperatura_media = temperatura_media;
		this.health = calculatehealth();
		this.promotion = promotion;
	}

	public cold_drink(String primary_key) {
		super(primary_key);
	}

	public void setAzucar(int azucar) {
		this.azucar = azucar;
		this.calculatehealth();
	}

	public int getAzucar() {
		return this.azucar;
	}

	public void setTemperatura_media(int temperatura_media) {
		this.temperatura_media = temperatura_media;
	}

	public int getTemperatura_media() {
		return this.temperatura_media;
	}

	public void setPromotion(fecha promotion) {
		this.promotion = promotion;
	}

	public fecha getPromotion() {
		return this.promotion;
	}

	public String calculatehealth() {
		if (this.azucar >= 74) {
			health = "Es una bebida que supera los niveles de az�car permitidos para el consumo diario";
		} else if (this.azucar >= 50) {
			health = "Esta bebida tiene un alto nivel de az�car";
		} else if (this.azucar >= 25) {
			health = "Nivel moderado de az�car";
		} else {
			health = "Bebida saludable y baja en az�cares";
		}
		return health;
	}

	public String toString() {
		return "Consumici�n " + getNombre() + "\nFecha de alta " + getFinicio() + "\nFecha de retirada "
				+ getFretirada() + "\nMedia al dia " + getMedia_cons() + "\nKarma " + getKarma() + "\nN�mero de az�car "
				+ azucar + " gramos\nTemperatura " + temperatura_media + " �C\n" + this.health
				+ "\nAprovecha su promoci�n el dia: " + getPromotion();
	}

}
