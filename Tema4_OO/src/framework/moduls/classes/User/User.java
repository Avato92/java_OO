package framework.moduls.classes.User;

public class User {
	private String name;
	private String pasword;

	public User(String name, String pasword) {
		this.name = name;
		this.pasword = pasword;
	}

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getPasword() {
		return this.pasword;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPasword(String pasword) {
		this.pasword = pasword;
	}

	public String toString() {
		return getName();
	}
}
