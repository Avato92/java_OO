package framework.moduls.classes.order;

import java.util.Comparator;

import framework.moduls.classes.consumiciones;

public class Order_entry_date implements Comparator<consumiciones> {

	public int compare(consumiciones con1, consumiciones con2) {
		return (con2.getFinicio().comparedate(con1.getFinicio()));
	}
}