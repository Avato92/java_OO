package framework.moduls.classes.order;

import java.util.Comparator;

import framework.moduls.classes.consumiciones;

public class Ordermedia implements Comparator<consumiciones> {

	public int compare(consumiciones con1, consumiciones con2) {
		if (con1.getMedia_cons() > con2.getMedia_cons()) {
			return 1;
		}
		if (con1.getMedia_cons() < con2.getMedia_cons()) {
			return -1;
		}
		return 0;
	}

}
