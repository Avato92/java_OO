package framework.moduls.classes.order;

import java.util.Comparator;

import framework.moduls.classes.consumiciones;

public class Ordername implements Comparator<consumiciones> {

	public int compare(consumiciones con1, consumiciones con2) {
		if (con1.getNombre().compareTo(con2.getNombre()) > 0) {
			return 1;
		}
		if (con1.getNombre().compareTo(con2.getNombre()) < 0) {
			return -1;
		}
		return 0;
	}

}
