package framework.moduls.classes.dummies;

import framework.clases.fecha;
import framework.moduls.classes.cold_drink;
import framework.moduls.classes.comida;
import framework.moduls.classes.hot_drink;
import framework.moduls.classes.singleton;
import framework.moduls.utils.funtions_find;
import framework.utils.funciones;

public class dummies {

	public static String food_name_dummies() {
		String food_name = "";
		String[] food = { "Miniburguer", "Champi�ones con jam�n", "Calamares a la romana", "Alcachofas con longaniza",
				"Patatas al horno", "Sepia", "Higado", "Tostadas", "Rulo de cabra", "Ensalada valenciana",
				"Ensalada de rulo de cabra", "Parrillada de verduras", "Arroz al horno", "Pimientos rellenos",
				"Emperador", "Entrecot", "Solomillo", "Lomo, huevo y patatas", "Merluza", "Morro de cerdo",
				"Chuletas de cordero", "Chuletillas de lechal", "Brocheta de gamba", "Croquetas de langostino",
				"Croquetas de chipir�n", "Croquetas de bacalao", "Ensaladilla de sepia", "Ensaladilla rusa", "Hummus",
				"Hummus de lentejas", "Arroz al curry", "Macarrones bolo�esa", "Spagettis carbonara" };
		int position = (int) (Math.random() * 300) % 33;
		food_name = food[position];
		return food_name;
	}

	public static String cold_drink_name_dummies() {
		String cold_drink_name = "";
		String[] cold_drink = { "Coca-Cola", "Fanta naranja", "Fanta lim�n", "Trina", "BitterKas", "Nestea", "Sprite",
				"Tinto de verano", "T�nica", "Agua grande", "Agua peque�a", "Estrella Damm", "Estrella Galicia",
				"Mahou Cl�sica", "Mahou 5 estrellas", "Mahou Barrica", "Copa de vino tinto", "Copa de vino blanco",
				"Copa de vino rosado", "Martini blanco", "Martini rojo", "Vermouth", "Gin-tonic", "Gin-lemon",
				"Vodka con limon", "Gin-sprite", "Gin-Ginger ale", "Ginger ale", "Vodka con coca-cola",
				"Whisky con lim�n", "Whisky con naranja", "Whisky con cocacola", "Ron-cola", "Ron-lima" };
		int position = (int) (Math.random() * 300) % 33;
		cold_drink_name = cold_drink[position];
		return cold_drink_name;
	}

	public static String hot_drink_name_dummies() {
		String hot_drink_name = "";
		String[] hot_drink = { "Caf�", "Cortado", "Bomb�n", "Cafe con leche", "Carajillo", "Carajillo de whisky",
				"Carajillo de ron", "Carajillo de bailys", "Capuccino", "Colacao", "Chocolate", "T� verde", "T� rojo",
				"T� negro", "T� blanco", "T� azul", "Tomillo", "Manzanilla", "Poleo-Menta", "Salvia", "Caf� caramelo",
				"Caf� Irland�s", "Caf� descafeinado de m�quina", "Cortado descafeinado de m�quina",
				"Caf� descafeinado de sobre", "Bomb�n descafeinado de m�quina", "Bomb�n descafeinado de sobre",
				"Cafe con leche descafeinado de m�quina", "Caf� con leche descafeinado de sobre",
				"Carajillo descafeinado", "Carajillo de whisky descafeinado", "Carajillo de ron descafeinado",
				"Carajillo de baylis descafeinado" };
		int position = (int) (Math.random() * 300) % 33;
		hot_drink_name = hot_drink[position];
		return hot_drink_name;
	}

	public static String primary_key_dummies() {
		String primary_key = "";
		String[] primary_key_word = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "�", "O",
				"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		String[] primary_key_number = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10" };
		int position = (int) (Math.random() * 300) % 27;
		int position2 = (int) (Math.random() * 300) % 10;
		primary_key = primary_key_word[position] + primary_key_number[position2];
		return primary_key;
	}

	public static fecha finicio_dummies() {
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		while (year < 1950 || year > 2000) {
			year = 1900 + ((((((int) (Math.random() * 1000)) % 10) % 5) + 5) * 10)
					+ (((int) (Math.random() * 1000)) % 10) + 1;
		}
		fecha finicio = new fecha(day, month, year);
		return finicio;
	}

	public static fecha fretirada_dummies() {
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		while (year < 2000 || year > 3000) {
			year = 2000 + ((((((int) (Math.random() * 1000)) % 10) % 5) + 5) * 10)
					+ (((int) (Math.random() * 1000)) % 10) + 1;
		}
		fecha fretirada = new fecha(day, month, year);
		return fretirada;

	}

	public static fecha promotion_date_dummies() {
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		while (year < 2000 || year > 3000) {
			year = 2000 + ((((((int) (Math.random() * 1000)) % 10) % 5) + 5) * 10)
					+ (((int) (Math.random() * 1000)) % 10) + 1;
		}
		fecha fretirada = new fecha(day, month, year);
		return fretirada;

	}

	public static int media_cons_dummies() {
		int media = (int) (Math.random() * 300) % 100;
		return media;
	}

	public static String karma_dummies() {
		String karma = "Plato estrella";
		return karma;
	}

	public static int num_ing_dummies() {
		int num_ing = (int) (Math.random() * 100) % 10;
		return num_ing;
	}

	public static int calories_dummies() {
		int calories = (int) (Math.random() * 200) % 100;
		return calories;
	}

	public static int valoration_dummies() {
		int valoration = 0;
		valoration = (int) (Math.random() * 10 + 1);
		return valoration;

	}

	public static String mark_dummies() {
		String mark = "";
		if (valoration_dummies() >= 10) {
			mark = "Matr�cula";
		} else if (valoration_dummies() >= 8) {
			mark = "Sobresaliente";
		} else if (valoration_dummies() == 7) {
			mark = "Notable";
		} else if (valoration_dummies() >= 5) {
			mark = "Suficiente";
		} else {
			mark = "Suspenso";
		}

		return mark;
	}

	public static String promotion_dummies() {
		String promotion = "No hay promociones v�lidas";
		return promotion;
	}

	public static int sugar_dummies() {
		int sugar = (int) (Math.random() * (0 - 200) - 200);
		return sugar;
	}

	public static int temperature_dummies() {
		int temp = (int) (Math.random() * (0 - 10) - 10);
		return temp;
	}

	public static String health_dummies() {
		String health = "Contiene una cantida de az�car adecuado";
		return health;
	}

	public static int hot_temperature_dummies() {
		int temp = (int) (Math.random() * (14 - 36) - 36);
		return temp;
	}

	public static String with_dummies() {
		String sugar = "Az�car";
		return sugar;
	}

	public static String cafeine_dummies() {
		String cafeine = "Descafeinado";
		return cafeine;
	}

	public static void make_food_dummie() {
		for (int i = 0; i < 5; i++) {
			comida con1 = new comida(food_name_dummies(), primary_key_dummies(), finicio_dummies(), fretirada_dummies(),
					media_cons_dummies(), num_ing_dummies(), calories_dummies(), valoration_dummies());
			singleton.consumption_food.add(con1);
		}
	}

	public static void make_cold_drink_dummie() {
		for (int i = 0; i < 5; i++) {
			cold_drink con1 = new cold_drink(cold_drink_name_dummies(), primary_key_dummies(), finicio_dummies(),
					fretirada_dummies(), media_cons_dummies(), sugar_dummies(), temperature_dummies(),
					promotion_date_dummies());
			singleton.consumption_cold_drink.add(con1);
		}
	}

	public static void make_hot_drink_dummie() {
		for (int i = 0; i < 5; i++) {
			hot_drink con1 = new hot_drink(hot_drink_name_dummies(), primary_key_dummies(), finicio_dummies(),
					fretirada_dummies(), media_cons_dummies(), hot_temperature_dummies(), with_dummies(),
					cafeine_dummies());
			singleton.consumption_hot_drink.add(con1);
		}
	}

	public static void work_dummies() {
		dummies.make_food_dummie();
		dummies.make_cold_drink_dummie();
		dummies.make_hot_drink_dummie();
	}

	public static String[] generate_vector_consumption(int op) {
		comida con1 = null;
		cold_drink con2 = null;
		hot_drink con3 = null;
		String cad = "";
		String[] consumption = null;
		int arraylist = 0;

		if (op == 0) {
			arraylist = singleton.consumption_food.size();
			consumption = new String[arraylist];
			for (int i = 0; i < arraylist; i++) {
				con1 = singleton.consumption_food.get(i);
				cad = con1.getPrimary_key() + ": " + con1.getNombre();
				consumption[i] = cad;
			}

		} else if (op == 1) {
			arraylist = singleton.consumption_cold_drink.size();
			consumption = new String[arraylist];
			for (int i = 0; i < arraylist; i++) {
				con2 = singleton.consumption_cold_drink.get(i);
				cad = con2.getPrimary_key() + ": " + con2.getNombre();
				consumption[i] = cad;
			}

		} else if (op == 2) {
			arraylist = singleton.consumption_hot_drink.size();
			consumption = new String[arraylist];
			for (int i = 0; i < arraylist; i++) {
				con3 = singleton.consumption_hot_drink.get(i);
				cad = con3.getPrimary_key() + ": " + con3.getNombre();
				consumption[i] = cad;
			}
		}
		return consumption;
	}

	public static int combo_find(int i) {
		int position = -1;
		String[] arrayname = null;
		String[] consumption = null;
		comida con1 = null;
		cold_drink con2 = null;
		hot_drink con3 = null;
		String search = "", primary_key = "";
		if (i == 0) {
			consumption = generate_vector_consumption(i);
			search = funciones.combomenu("Elija que consumici�n quiere", "Consumici�n", consumption);
			if (search != "") {
				arrayname = search.split(":");
				primary_key = arrayname[0];
				con1 = new comida(primary_key);
				position = funtions_find.find_food(con1);
			}

		} else if (i == 1) {
			consumption = generate_vector_consumption(i);
			search = funciones.combomenu("Elija que consumici�n quiere", "Consumici�n", consumption);
			if (search != "") {
				arrayname = search.split(":");
				primary_key = arrayname[0];
				con2 = new cold_drink(primary_key);
				position = funtions_find.find_cold_drink(con2);
			}
		} else if (i == 2) {
			consumption = generate_vector_consumption(i);
			search = funciones.combomenu("Elija que consumici�n quiere", "Consumici�n", consumption);
			if (search != "") {
				arrayname = search.split(":");
				primary_key = arrayname[0];
				con3 = new hot_drink(primary_key);
				position = funtions_find.find_hot_drink(con3);
			}
		}
		return position;
	}
}
