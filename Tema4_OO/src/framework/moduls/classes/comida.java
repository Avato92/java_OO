package framework.moduls.classes;

import framework.clases.fecha;

public class comida extends consumiciones {
	private int n_ingr;
	private int calorias;
	private int valoration;
	private String media;
	private String promotion;

	public comida(String nombre, String primary_key, fecha finicio, fecha fretirada, int media_cons, int n_ingr,
			int calorias, int valoration) {
		super(nombre, primary_key, finicio, fretirada, media_cons);
		this.n_ingr = n_ingr;
		this.calorias = calorias;
		this.valoration = valoration;
		calculate_media();
		this.promotion = promotion(finicio);
	}

	public comida(String primary_key) {
		super(primary_key);
	}

	public void setN_ingr(int n_ingr) {
		this.n_ingr = n_ingr;
	}

	public int getN_ingr() {
		return this.n_ingr;
	}

	public void setCalorias(int calorias) {
		this.calorias = calorias;
	}

	public int getCalorias() {
		return this.calorias;
	}

	public void setValoration(int valoration) {
		this.valoration = valoration;
		calculate_media();
	}

	public int getValoration() {
		return this.valoration;
	}

	public void calculate_media() {
		if (valoration >= 10) {
			media = "Matr�cula";
		} else if (valoration >= 8) {
			media = "Sobresaliente";
		} else if (valoration == 7) {
			media = "Notable";
		} else if (valoration >= 5) {
			media = "Suficiente";
		} else {
			media = "Suspenso";
		}
	}

	public String promotion(fecha getFinicio) {
		int day = 0, month = 0;
		String[] arraydate = null;
		String edate = null, promotion = null;
		edate = getFinicio.toString();
		arraydate = edate.split("/");

		day = Integer.parseInt(arraydate[0]);
		month = Integer.parseInt(arraydate[1]);
		if ((month == 1) && ((day >= 1) || (day <= 6))) {
			promotion = "Promociones de navidad 20% de descuento en comidas navide�as";
		} else if ((month == 2) && (day == 14)) {
			promotion = "Promoci�n de San Valent�n, 30% de descuento en cenas para dos personas";
		} else if ((month == 10) && (day == 31)) {
			promotion = "Promoci�n de Haloween ven solo a comer y pasa un dia de miedo con nuestro descuento del 50%";
		} else {
			promotion = "No hay promociones v�lidas en esas fechas";
		}
		return promotion;
	}

	public String getMedia() {
		return media;
	}

	public String toString() {
		return "Consumici�n " + getNombre() + "\nFecha de alta " + getFinicio() + "\nFecha de retirada "
				+ getFretirada() + "\nMedia al dia " + getMedia_cons() + "\nKarma " + getKarma()
				+ "\nN�mero de ingredientes " + n_ingr + "\nCalorias " + calorias + "\nValoraci�n " + valoration
				+ "\nResultado " + media + "\n" + promotion;
	}

	public String toString(String primary_key) {
		String cad = "";
		cad = cad + (super.getPrimary_key());
		return cad;
	}

}
