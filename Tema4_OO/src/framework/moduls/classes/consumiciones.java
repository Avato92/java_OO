package framework.moduls.classes;

import framework.clases.fecha;

public abstract class consumiciones implements Comparable<consumiciones> {

	// Atributes
	private String nombre;
	private String primary_key;
	private fecha finicio;
	private fecha fretirada;
	private int media_cons;
	private String karma;

	// building
	public consumiciones(String Nombre, String primary_key, fecha finicio, fecha fretirada, int media_cons) {
		this.nombre = Nombre;
		this.primary_key = primary_key;
		this.finicio = finicio;
		this.fretirada = fretirada;
		this.media_cons = media_cons;
		this.karma = calculate_demanda();
	}

	// primary key building
	public consumiciones(String primary_key) {
		this.primary_key = primary_key;
	}

	// Getters & Setters

	public void setNombre(String Nombre) {
		this.nombre = Nombre;
	}

	public void setPrimary_key(String primary_key) {
		this.primary_key = primary_key;
	}

	public void setFinicio(fecha finicio) {
		this.finicio = finicio;
	}

	public void setFretirada(fecha fretirada) {
		this.fretirada = fretirada;
	}

	public void setMedia_cons(int media_cons) {
		this.media_cons = media_cons;
		this.calculate_demanda();
	}

	public int getMedia_cons() {
		return media_cons;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPrimary_key() {
		return primary_key;
	}

	public fecha getFinicio() {
		return finicio;
	}

	public fecha getFretirada() {
		return fretirada;
	}

	public String getKarma() {
		return karma;
	}

	public String calculate_demanda() {
		if (this.media_cons >= 20) {
			karma = "Plato estrella";
		} else if ((this.media_cons < 20) && (this.media_cons > 10)) {
			karma = "Plato normal";
		} else if ((this.media_cons <= 10)) {
			karma = "Plato poco demandado";
		}
		return karma;
	}

	// ToString
	public abstract String toString();

	// ToString_P_K
	public String toString(String primary_key) {
		String cad = "";

		cad = cad + (this.getPrimary_key());
		return cad;
	}

	public int compareTo(consumiciones o) {
		return 0;
	}
}
