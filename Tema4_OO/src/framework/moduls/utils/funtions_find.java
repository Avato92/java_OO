package framework.moduls.utils;

import framework.moduls.classes.cold_drink;
import framework.moduls.classes.comida;
import framework.moduls.classes.hot_drink;
import framework.moduls.classes.singleton;
import framework.moduls.classes.User.User;

public class funtions_find {
	public static int find_food(comida con1) {
		int position = 0;
		for (int i = 0; i < singleton.consumption_food.size(); i++) {
			if (singleton.consumption_food.get(i).getPrimary_key().equals(con1.getPrimary_key())) {
				position = i;
				break;
			} else {
				position = -1;
			}
		}
		return position;

	}

	public static int find_cold_drink(cold_drink con1) {
		int position = 0;
		for (int i = 0; i < singleton.consumption_cold_drink.size(); i++) {
			if (singleton.consumption_cold_drink.get(i).getPrimary_key().equals(con1.getPrimary_key())) {
				position = i;
				break;
			} else
				position = -1;
		}
		return position;
	}

	public static int find_hot_drink(hot_drink con1) {
		int position = 0;
		for (int i = 0; i < singleton.consumption_hot_drink.size(); i++) {
			if (singleton.consumption_hot_drink.get(i).getPrimary_key().equals(con1.getPrimary_key())) {
				position = i;
				break;
			} else
				position = -1;
		}
		return position;
	}

	public static int find_user(User u1) {
		int position = -1;
		for (int i = 0; i < singleton.user.size(); i++) {
			if (singleton.user.get(i).getName().equals(u1.getName())) {
				position = i;
				break;
			} else
				position = -1;
		}
		return position;
	}

	public static boolean equal_pasword(User u1) {
		boolean result = false;
		if (u1.getName().equals(u1.getPasword())) {
			result = false;
		} else {
			result = true;
		}
		return result;

	}

	public static int find_pasword(User u1) {
		int position = -1;
		for (int i = 0; i < singleton.user_complete.size(); i++) {
			if (singleton.user_complete.get(i).getName().equals(u1.getName())) {
				if (singleton.user_complete.get(i).getPasword().equals(u1.getPasword())) {
					position = 1;
				} else {
					position = -1;
				}
			} else {
				position = -1;
			}
		}
		return position;
	}

}
