package framework.moduls.utils;

import java.util.Collections;

import framework.clases.fecha;
import framework.moduls.classes.cold_drink;
import framework.moduls.classes.comida;
import framework.moduls.classes.hot_drink;
import framework.moduls.classes.singleton;
import framework.moduls.classes.dummies.dummies;
import framework.moduls.classes.order.Order_entry_date;
import framework.moduls.classes.order.Ordermedia;
import framework.moduls.classes.order.Ordername;
import framework.moduls.utils.utils_language.Language_utils;
import framework.utils.funciones;

public class funtions_class {
	public static void create(int i, int lang) {
		Language_utils language = new Language_utils(lang);

		if (i == 0) {
			String primary_key = ask_data_consuptions.ask_primary_key(lang);
			String Nombre = ask_data_consuptions.ask_name(lang);
			fecha finicio = ask_data_consuptions.ask_entry_date(lang);
			fecha fretirada = ask_data_consuptions.ask_exit_date(finicio, lang);
			int media_cons = funciones.valida_Int(language.message_media(lang), language.title_average(lang));
			int n_ingr = funciones.valida_Int(language.message_ingr(lang), language.title_ingr(lang));
			int calorias = funciones.valida_Int(language.message_calories(lang), language.title_calories(lang));
			int valoration = ask_data_consuptions.ask_valoration(lang);

			comida con1 = new comida(Nombre, primary_key, finicio, fretirada, media_cons, n_ingr, calorias, valoration);
			singleton.consumption_food.add(con1);
			funciones.print(language.success_food(lang));
		}

		else if (i == 1) {
			String primary_key = ask_data_consuptions.ask_primary_key(lang);
			String Nombre = ask_data_consuptions.ask_name(lang);
			fecha finicio = ask_data_consuptions.ask_entry_date(lang);
			fecha fretirada = ask_data_consuptions.ask_exit_date(finicio, lang);
			int media_cons = funciones.valida_Int(language.message_media(lang), language.title_average(lang));
			int azucar = funciones.valida_Int(language.message_sugar(lang), language.title_sugar(lang));
			int temperatura_media = funciones.valida_Int(language.message_average_temp(lang),
					language.title_average_temp(lang));
			fecha promotion = ask_data_consuptions.promotion_date(finicio, fretirada);

			cold_drink con1 = new cold_drink(Nombre, primary_key, finicio, fretirada, media_cons, azucar,
					temperatura_media, promotion);
			singleton.consumption_cold_drink.add(con1);
			funciones.print(language.success_cold_drink(lang));
		}

		else if (i == 2) {
			String primary_key = ask_data_consuptions.ask_primary_key(lang);
			String Nombre = ask_data_consuptions.ask_name(lang);
			fecha finicio = ask_data_consuptions.ask_entry_date(lang);
			fecha fretirada = ask_data_consuptions.ask_exit_date(finicio, lang);
			int media_cons = funciones.valida_Int(language.message_media(lang), language.title_average(lang));
			int temperature = funciones.valida_Int(language.message_temperature_hot_drink(lang),
					language.title_average_temp(lang));
			String sugar = funciones.validaString(language.message_sugar_hot_drink(lang), language.title_sugar(lang));
			String cafeine = funciones.validaString(language.message_cafeine(lang), language.title_cafeine(lang));

			hot_drink con1 = new hot_drink(Nombre, primary_key, finicio, fretirada, media_cons, temperature, sugar,
					cafeine);
			singleton.consumption_hot_drink.add(con1);
			funciones.print(language.hot_drink_success(lang));
		}
	}

	public static void update(int i, int lang) {
		Language_utils language = new Language_utils(lang);
		int location = 0;
		comida con1 = null;
		cold_drink con2 = null;
		hot_drink con3 = null;
		String[] operaciones = language.food_options(lang);
		String[] operaciones2 = language.cold_drink_options(lang);
		String[] operaciones3 = language.hot_drink__options(lang);
		int op = 0;
		if (i == 0) {
			if (singleton.consumption_food.isEmpty()) {
				funciones.print(language.error_empty(lang));
			} else {
				if (location != -1) {
					location = dummies.combo_find(i);
					con1 = singleton.consumption_food.get(location);
					op = funciones.menu_botones(operaciones, language.message_update(lang), "Actualizar");
					switch (op) { // TODO
					// traducir desde aqu�
					case 0:
						con1.setNombre(ask_data_consuptions.ask_name(lang));
						break;
					case 1:
						con1.setFinicio(ask_data_consuptions.ask_entry_date(lang));
						break;
					case 2:
						con1.setFretirada(ask_data_consuptions.ask_exit_date(con1.getFinicio(), lang));
						break;
					case 3:
						con1.setMedia_cons((funciones.valida_Int("Media de veces al dia que sale el plato", "Media")));
						break;
					case 4:
						((comida) con1)
								.setN_ingr((funciones.valida_Int("�Cu�ntos ingredientes tiene?", "Ingredientes")));
						break;
					case 5:
						((comida) con1).setCalorias(funciones.valida_Int("�Cu�ntas calorias tiene?", "Calorias"));
						break;
					case 6:
						((comida) con1).setValoration(ask_data_consuptions.ask_valoration(lang));
						break;
					}

				} else {
					funciones.print("No hay datos equivalentes");
				}
			}
		} else if (i == 1) {

			if (singleton.consumption_cold_drink.isEmpty()) {
				funciones.print("No se puede actualizar, porque no hay datos creados");
			} else {
				location = dummies.combo_find(i);
				if (location != -1) {
					con2 = singleton.consumption_cold_drink.get(location);
					op = funciones.menu_botones(operaciones2, language.message_update(lang), "Actualizar");
					switch (op) {
					case 0:
						con2.setNombre(ask_data_consuptions.ask_name(lang));
						break;
					case 1:
						con2.setFinicio(ask_data_consuptions.ask_entry_date(lang));
						break;
					case 2:
						con2.setFretirada(ask_data_consuptions.ask_exit_date(con2.getFinicio(), lang));
						break;
					case 3:
						con2.setMedia_cons((funciones.valida_Int("Media de veces al dia que sale el plato", "Media")));
						break;
					case 4:
						((cold_drink) con2)
								.setAzucar(funciones.valida_Int("�Cu�nto azucar tiene la bebida?", "Azucar"));
						break;
					case 5:
						((cold_drink) con2).setTemperatura_media(
								funciones.valida_Int("�A que temperatura debe estar la bebida?", "Temperatura"));
						break;

					}

				} else {
					funciones.print("No hay datos equivalentes");
				}
			}
		} else if (i == 2) {
			if (singleton.consumption_hot_drink.isEmpty()) {
				funciones.print("No se puede actualizar, porque no hay datos creados");
			} else {
				location = dummies.combo_find(i);
				if (location != -1) {
					con3 = singleton.consumption_hot_drink.get(location);
					op = funciones.menu_botones(operaciones3, language.message_update(lang), "Actualizar");
					switch (op) {
					case 0:
						con3.setNombre(ask_data_consuptions.ask_name(lang));
						break;
					case 1:
						con3.setFinicio(ask_data_consuptions.ask_entry_date(lang));
						break;
					case 2:
						con3.setFretirada(ask_data_consuptions.ask_exit_date(con3.getFinicio(), lang));
						break;
					case 3:
						con3.setMedia_cons((funciones.valida_Int("Media de veces al dia que sale el plato", "Media")));
						break;
					case 4:
						((hot_drink) con3).setTemperature(
								funciones.valida_Int("Temperatura de la bebida caliente", "Temperatura"));
						break;
					case 5:
						((hot_drink) con3)
								.setSugar(funciones.validaString("�Con que acompa�as la bebida caliente?", "Azucar"));
						break;
					case 6:
						((hot_drink) con3).setCafeine(funciones.validaString("Es normal o descafeinado?", "Cafeina"));
						break;

					}

				} else {
					funciones.print("No hay datos equivalentes");
				}
			}
		}

	}

	public static void read(int i) {
		comida con1 = null;
		hot_drink con3 = null;
		cold_drink con2 = null;
		int op = 0, position = 0;
		String[] read = { "Todo", "Elegir consumici�n" };

		op = funciones.menu_botones(read, "�Que quieres leer?", "Leer");
		switch (op) {
		case 0:
			if (i == 0) {
				if (singleton.consumption_food.isEmpty()) {
					funciones.print("No hay datos guardados");
				} else {
					for (int j = 0; j < singleton.consumption_food.size(); j++) {
						funciones.print(singleton.consumption_food.get(j).toString());
					}
				}
			} else if (i == 1) {
				if (singleton.consumption_cold_drink.isEmpty()) {
					funciones.print("No hay datos guardados");
				} else {
					for (int j = 0; j < singleton.consumption_cold_drink.size(); j++) {
						funciones.print(singleton.consumption_cold_drink.get(j).toString());
					}
				}
			} else if (i == 2) {
				if (singleton.consumption_hot_drink.isEmpty()) {
					funciones.print("No hay datos guardados");
				} else {
					for (int j = 0; j < singleton.consumption_hot_drink.size(); j++) {
						funciones.print(singleton.consumption_hot_drink.get(j).toString());
					}
				}
			}
			break;
		case 1:

			if (i == 0) {
				if (singleton.consumption_food.isEmpty()) {
					funciones.print("No hay datos creados");
				} else {
					position = dummies.combo_find(i);
					if (position != -1) {
						con1 = singleton.consumption_food.get(position);
						funciones.print(con1.toString());
					} else {
						funciones.print("No se encuentran datos equivalentes");
					}
				}

			} else if (i == 1) {
				if (singleton.consumption_cold_drink.isEmpty()) {
					funciones.print("No hay datos creados");
				} else {
					position = dummies.combo_find(i);
					if (position != -1) {
						con2 = singleton.consumption_cold_drink.get(position);
						funciones.print(con2.toString());
					} else {
						funciones.print("No se encuentran datos equivalentes");
					}
				}
			} else if (i == 2) {
				if (singleton.consumption_hot_drink.isEmpty()) {
					funciones.print("No hay datos creados");
				} else {
					position = dummies.combo_find(i);
					if (position != -1) {
						con3 = singleton.consumption_hot_drink.get(position);
						funciones.print(con3.toString());
					} else {
						funciones.print("No se encuentran datos equivalentes");
					}
				}
			}
		}
	}

	public static void delete(int i) {
		int position = 0;

		if (i == 0) {
			position = dummies.combo_find(i);
			if (position != -1) {
				singleton.consumption_food.remove(position);
			} else {
				funciones.print("No se encuentra la clave");
			}

		} else if (i == 1) {
			position = dummies.combo_find(i);
			if (position != -1) {
				singleton.consumption_cold_drink.remove(position);
			} else {
				funciones.print("No se encuentra la clave");
			}

		} else if (i == 2) {
			position = dummies.combo_find(i);
			if (position != -1) {
				singleton.consumption_hot_drink.remove(position);
			} else {
				funciones.print("No se encuentra la clave");
			}
		}
	}

	public static void order(int i) {
		String[] options = { "Nombre", "Fecha de entrada", "Media" };
		int op = 0;

		if (i == 0) {
			if (singleton.consumption_food.isEmpty()) {
				funciones.print("No hay datos para ordenar");
			} else {
				op = funciones.menu_botones(options, "�C�mo lo quieres ordenar?", "Orden");
				switch (op) {
				case 0:
					Collections.sort(singleton.consumption_food, new Ordername());
					break;
				case 1:
					Collections.sort(singleton.consumption_food, new Order_entry_date());
					break;
				case 2:
					Collections.sort(singleton.consumption_food, new Ordermedia());
					break;
				default:
					break;
				}
			}
		} else if (i == 1) {
			if (singleton.consumption_cold_drink.isEmpty()) {
				funciones.print("No hay datos para ordenar");
			} else {
				op = funciones.menu_botones(options, "�C�mo lo quieres ordenar?", "Orden");
				switch (op) {
				case 0:
					Collections.sort(singleton.consumption_cold_drink, new Ordername());
					break;
				case 1:
					Collections.sort(singleton.consumption_cold_drink, new Order_entry_date());
					break;
				case 2:
					Collections.sort(singleton.consumption_cold_drink, new Ordermedia());
					break;
				default:
					break;
				}
			}
		} else if (i == 2) {
			if (singleton.consumption_hot_drink.isEmpty()) {
				funciones.print("No hay datos para ordenar");
			} else {
				op = funciones.menu_botones(options, "�C�mo lo quieres ordenar?", "Orden");
				switch (op) {
				case 0:
					Collections.sort(singleton.consumption_hot_drink, new Ordername());
					break;
				case 1:
					Collections.sort(singleton.consumption_hot_drink, new Order_entry_date());
					break;
				case 2:
					Collections.sort(singleton.consumption_hot_drink, new Ordermedia());
					break;
				default:
					break;
				}
			}
		}
	}

}
