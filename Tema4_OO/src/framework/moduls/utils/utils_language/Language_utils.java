package framework.moduls.utils.utils_language;

public class Language_utils {
	public static Language_utils instance;
	private int lang;

	public static Language_utils getInstance() {
		if (instance == null) {
			instance = new Language_utils(0);

		}
		return instance;
	}

	public Language_utils(int lang) {
		this.lang = lang;
	}

	public int getLang() {
		return lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public String message_primary_key(int i) {
		String primary_key = "";
		switch (i) {
		case 0:
			primary_key = "Introduce la clave del producto";
			break;
		case 1:
			primary_key = "Introdueix la clau del producte";
			break;
		case 2:
			primary_key = "Insert the product key";
			break;
		default:
			primary_key = "Introduce la clave del producto";
			break;
		}
		return primary_key;
	}

	public String title_primary_key(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Clave";
			break;
		case 1:
			title = "Clau";
			break;
		case 2:
			title = "Key";
			break;
		default:
			title = "Clave";
		}
		return title;
	}

	public String primary_key_error(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "La clave ya est� en uso";
			break;
		case 1:
			error = "La clau ja est� en �s";
			break;
		case 2:
			error = "The key is alredy in use";
			break;
		default:
			error = "La clave ya est� en uso";
			break;
		}
		return error;
	}

	public String new_primary_key(int i) {
		String succes = "";
		switch (i) {
		case 0:
			succes = "La clave del producto no existe, procedemos a su creaci�n";
			break;
		case 1:
			succes = "La clau del producte no existeix, procedim a la seva creaci�";
			break;
		case 2:
			succes = "The key of the product doesn't exist, we proceed to its creation";
			break;
		default:
			succes = "La clave del producto no existe, procedemos a su creaci�n";
			break;
		}
		return succes;
	}

	public String message_ask_name(int i) {
		String name = "";
		switch (i) {
		case 0:
			name = "Introduzca el nombre del producto";
			break;
		case 1:
			name = "Introdueixi el nom del producte";
			break;
		case 2:
			name = "Write the name of the product";
			break;
		default:
			name = "Introduzca el nombre del producto";
			break;
		}
		return name;
	}

	public String title_ask_name(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Nombre";
			break;
		case 1:
			title = "Nom";
			break;
		case 2:
			title = "Name";
			break;
		default:
			title = "Nombre";
			break;
		}
		return title;
	}

	public String error_name(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "No has introducido un nombre v�lido";
			break;
		case 1:
			error = "No has introduit un nom v�lid";
			break;
		case 2:
			error = "You haven't entered a valid name";
			break;
		default:
			error = "No has introducido un nombre v�lido";
			break;
		}
		return error;
	}

	public String message_ask_entry_date(int i) {
		String date = "";
		switch (i) {
		case 0:
			date = "Introduce la fecha de entrada del producto";
			break;
		case 1:
			date = "Introdueixi la data d'entrada del producte";
			break;
		case 2:
			date = "Insert the product entry date";
			break;
		default:
			date = "Introduzca la fecha de entrada del producto";
			break;
		}
		return date;
	}

	public String title_ask_entry_date(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Entrada producto";
			break;
		case 1:
			title = "Entrada producte";
			break;
		case 2:
			title = "Entry date";
			break;
		default:
			title = "Entrada producto";
			break;
		}
		return title;
	}

	public String eror_date(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "Fecha no v�lida";
			break;
		case 1:
			error = "Data no v�lida";
			break;
		case 2:
			error = "Not valid date";
			break;
		default:
			error = "Fecha no v�lida";
			break;
		}
		return error;
	}

	public String message_ask_exit_date(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "Introduzca la fecha de salida del producto";
			break;
		case 1:
			message = "Introdueixi la data d'eixida del producte";
			break;
		case 2:
			message = "Insert the product exit date";
			break;
		default:
			message = "Introduzca la fecha de salida del producto";
			break;
		}
		return message;
	}

	public String title_ask_exit_date(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Fecha de salida";
			break;
		case 1:
			title = "Data d'eixida";
			break;
		case 2:
			title = "Exit date";
			break;
		default:
			title = "Fecha de salida";
			break;
		}
		return title;
	}

	public String error_before_date(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "No puedes retirar un producto antes de introducirlo";
			break;
		case 1:
			error = "No pots retirar un producte abans d'introduir-lo";
			break;
		case 2:
			error = "You can't remove a product before entering it";
			break;
		default:
			error = "No puedes retirar un producto antes de introducirlo";
			break;
		}
		return error;
	}

	public String error_same_date(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "No puedes retirar un producto el mismo dia que se introduce";
			break;
		case 1:
			error = "No pots retirar un producte el mateix dia que l'introduixes";
			break;
		case 2:
			error = "You can't pick up a product the same day it is introduced";
			break;
		default:
			error = "No puedes retirar un producto el mismo dia que se introduce";
			break;
		}
		return error;
	}

	public String message_ask_valoration(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Que nota entre 0 y 10 le pones?";
			break;
		case 1:
			message = "Qu� nota entre 0 i 10 li poses?";
			break;
		case 2:
			message = "What mark between 0 and 10 do you put?";
			break;
		default:
			message = "�Qu� nota entre 0 y 10 le pones?";
			break;
		}
		return message;
	}

	public String title_ask_valoration(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Valoraci�n";
			break;
		case 1:
			title = "Valoraci�";
			break;
		case 2:
			title = "Valoration";
			break;
		default:
			title = "Valoraci�n";
			break;
		}
		return title;
	}

	public String error_valoration(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "No has introducido una nota v�lida";
			break;
		case 1:
			error = "No has introduit una nota v�lida";
			break;
		case 2:
			error = "You haven't entered a valid mark";
		default:
			error = "No has introducido una nota v�lida";
			break;
		}
		return error;
	}

	public String message_media(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Cu�l es la media de veces que sale la consumici�n al dia?";
			break;
		case 1:
			message = "Quina es la mitja de vegades que ix la consumici� al dia?";
			break;
		case 2:
			message = "What is the average number of times the consumption is served per day? ";
			break;
		default:
			message = "�Cu�l es la media de veces que sale la consumici�n al dia?";
			break;
		}
		return message;
	}

	public String title_average(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Media";
			break;
		case 1:
			title = "Mitja";
			break;
		case 2:
			title = "Average";
			break;
		default:
			title = "Media";
			break;
		}
		return title;
	}

	public String message_ingr(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Cu�ntos ingredientes tiene?";
			break;
		case 1:
			message = "Quants ingredients te?";
			break;
		case 2:
			message = "How many ingredients does it have?";
			break;
		default:
			message = "�Cu�ntos ingredientes tiene?";
			break;
		}
		return message;
	}

	public String title_ingr(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Ingredientes";
			break;
		case 1:
			title = "Ingredients";
			break;
		case 2:
			title = "Ingredients";
			break;
		default:
			title = "Ingredientes";
			break;
		}
		return title;
	}

	public String message_calories(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Cu�ntas calorias tiene?";
			break;
		case 1:
			message = "Qu�ntes calories te?";
			break;
		case 2:
			message = "How many calories does it have?";
			break;
		default:
			message = "�Cu�ntas calorias tiene?";
			break;
		}
		return message;
	}

	public String title_calories(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Calorias";
			break;
		case 1:
			title = "Calories";
			break;
		case 2:
			title = "Calories";
			break;
		default:
			title = "Calorias";
			break;
		}
		return title;
	}

	public String success_food(int i) {
		String success = "";
		switch (i) {
		case 0:
			success = "La comida ha sido creada";
			break;
		case 1:
			success = "El menjar ha sigut creat";
			break;
		case 2:
			success = "The food has been created";
			break;
		default:
			success = "La comida ha sido creada";
			break;
		}
		return success;

	}

	public String message_sugar(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Cu�nto az�car tiene esta bebida?";
			break;
		case 1:
			message = "Quant de sucre te esta beguda?";
			break;
		case 2:
			message = "How much sugar does this drink have?";
			break;
		default:
			message = "�Cu�nto az�car tiene esta bebida?";
			break;
		}
		return message;
	}

	public String title_sugar(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Az�car";
			break;
		case 1:
			title = "Sucre";
			break;
		case 2:
			title = "Sugar";
			break;
		default:
			title = "Az�car";
			break;
		}
		return title;
	}

	public String message_average_temp(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Qu� temperatura media tiene la bebida?";
			break;
		case 1:
			message = "Qu� temperatura mitja te la beguda?";
			break;
		case 2:
			message = "What is the average temperature of the drink?";
			break;
		default:
			message = "�Qu� temperatura media tiene la bebida?";
			break;
		}
		return message;
	}

	public String title_average_temp(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Temperatura";
			break;
		case 1:
			title = "Temperatura";
			break;
		case 2:
			title = "Temperature";
			break;
		default:
			title = "Temperatura";
			break;
		}
		return title;
	}

	public String success_cold_drink(int i) {
		String success = "";
		switch (i) {
		case 0:
			success = "La bebida fria ha sido creada";
			break;
		case 1:
			success = "La beguda freda ha sigut creada";
			break;
		case 2:
			success = "The cold drink has been created";
			break;
		default:
			success = "La bebida fria ha sido creada";
			break;
		}
		return success;
	}

	public String message_temperature_hot_drink(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Qu� temperatura tiene?";
			break;
		case 1:
			message = "Qu� temperatura te?";
			break;
		case 2:
			message = "What is the temperature of the hot drink?";
			break;
		default:
			message = "�Qu� temperatura tiene?";
			break;
		}
		return message;
	}

	public String message_sugar_hot_drink(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Con qu� acompa�a la bebida caliente?";
			break;
		case 1:
			message = "Amb qu� acompanya la beguda calenta?";
			break;
		case 2:
			message = "What goes with the hot drink?";
			break;
		default:
			message = "�Con qu� acompa�a la bebida caliente?";
			break;
		}
		return message;
	}

	public String message_cafeine(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Normal o descafeinado?";
			break;
		case 1:
			message = "Normal o descafeinat?";
			break;
		case 2:
			message = "Normal or decaffeinated";
			break;
		default:
			message = "�Normal o descafeinado?";
			break;
		}
		return message;
	}

	public String title_cafeine(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Cafeina";
			break;
		case 1:
			title = "Cafeina";
			break;
		case 2:
			title = "Cafeine";
			break;
		default:
			title = "Cafeina";
			break;
		}
		return title;
	}

	public String hot_drink_success(int i) {
		String success = "";
		switch (i) {
		case 0:
			success = "La bebida caliente ha sido creada";
			break;
		case 1:
			success = "La beguda calenta ha sigut creada";
			break;
		case 2:
			success = "The hot drink has been created";
			break;
		default:
			success = "La bebida caliente ha sido creada";
			break;
		}
		return success;
	}

	public String[] food_options(int i) {
		String[] options = { "", "", "", "", "", "", "" };
		switch (i) {
		case 0:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Ingredientes";
			options[5] = "Calorias";
			options[6] = "Valoraci�n";
			break;
		case 1:
			options[0] = "Nom";
			options[1] = "Data d' entrada";
			options[2] = "Data de retirada";
			options[3] = "Mitja";
			options[4] = "Ingredients";
			options[5] = "Calories";
			options[6] = "Valoraci�";
			break;
		case 2:
			options[0] = "Name";
			options[1] = "Entry date";
			options[2] = "Exit date";
			options[3] = "Average";
			options[4] = "Ingredients";
			options[5] = "Calories";
			options[6] = "Valoration";
			break;
		default:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Ingredientes";
			options[5] = "Calorias";
			options[6] = "Valoraci�n";
			break;
		}
		return options;
	}

	public String[] cold_drink_options(int i) {
		String[] options = { "", "", "", "", "", "" };
		switch (i) {
		case 0:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Az�car";
			options[5] = "Temperatura";
			break;
		case 1:
			options[0] = "Nom";
			options[1] = "Data d' entrada";
			options[2] = "Data de retirada";
			options[3] = "Mitja";
			options[4] = "Sucre";
			options[5] = "Temperatura";
			break;
		case 2:
			options[0] = "Name";
			options[1] = "Entry date";
			options[2] = "Exit date";
			options[3] = "Average";
			options[4] = "Sugar";
			options[5] = "Temperature";
			break;
		default:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Az�car";
			options[5] = "Temperatura";
			break;
		}
		return options;
	}

	public String[] hot_drink__options(int i) {
		String[] options = { "", "", "", "", "", "", "" };
		switch (i) {
		case 0:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Temperatura";
			options[5] = "Az�car";
			options[6] = "Cafeina";
			break;
		case 1:
			options[0] = "Nom";
			options[1] = "Data d' entrada";
			options[2] = "Data de retirada";
			options[3] = "Mitja";
			options[4] = "Temperatura";
			options[5] = "Sucre";
			options[6] = "Cafeina";
			break;
		case 2:
			options[0] = "Name";
			options[1] = "Entry date";
			options[2] = "Exit date";
			options[3] = "Average";
			options[4] = "Temperature";
			options[5] = "Sugar";
			options[6] = "Cafeine";
			break;
		default:
			options[0] = "Nombre";
			options[1] = "Fecha de entrada";
			options[2] = "Fecha de retirada";
			options[3] = "Media";
			options[4] = "Temperatura";
			options[5] = "Az�car";
			options[6] = "Cafeina";
			break;
		}
		return options;
	}

	public String error_empty(int i) {
		String error = "";
		switch (i) {
		case 0:
			error = "No hay datos";
			break;
		case 1:
			error = "No hi han dades";
			break;
		case 2:
			error = "Error 404: Data not found";
			break;
		default:
			error = "Error 404: No se encuentran datos";
			break;
		}
		return error;
	}

	public String message_update(int i) {
		String message = "";
		switch (i) {
		case 0:
			message = "�Qu� quieres modificar?";
			break;
		case 1:
			message = "Qu� vols actualitzar?";
			break;
		case 2:
			message = "What do you want update?";
			break;
		default:
			message = "�Qu� quieres modificar?";
			break;
		}
		return message;
	}
}
