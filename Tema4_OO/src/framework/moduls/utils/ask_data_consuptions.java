package framework.moduls.utils;

import framework.clases.fecha;
import framework.moduls.classes.singleton;
import framework.moduls.utils.utils_language.Language_utils;
import framework.utils.funciones;
import framework.utils.validate;

public class ask_data_consuptions {
	public static String ask_name(int lang) {
		Language_utils language = new Language_utils(lang);
		boolean result = false;
		String nombre = "";

		do {
			nombre = funciones.validaString(language.message_ask_name(lang), language.title_ask_name(lang));
			result = validate.validate_name(nombre);
			if (result == false) {
				funciones.print(language.error_name(lang));
			} else
				result = true;
		} while (result == false);
		return nombre;
	}

	public static fecha ask_entry_date(int lang) {
		Language_utils language = new Language_utils(lang);
		String edate = "";
		boolean result = false;
		fecha n = null;

		do {
			edate = funciones.validaString(language.message_ask_entry_date(lang), language.title_ask_entry_date(lang));
			result = validate.validate_date(edate);
			if (result == false) {
				funciones.print(language.eror_date(lang));
			} else {
				result = true;
				n = new fecha(edate);
				result = n.validate_date();
				if (result == false) {
					funciones.print(language.eror_date(lang));
				}
			}

		} while (result == false);

		return n;
	}

	public static fecha ask_exit_date(fecha finicio, int lang) {
		Language_utils language = new Language_utils(lang);
		boolean result = false;
		String fdate = "";
		fecha n = null;
		int compare = 0;
		do {
			fdate = funciones.validaString(language.message_ask_exit_date(lang), language.title_ask_exit_date(lang));
			result = validate.validate_date(fdate);
			if (result == false) {
				funciones.print(language.eror_date(lang));
			} else {
				result = true;
				n = new fecha(fdate);
				result = n.validate_date();
				if (result == false) {
					funciones.print(language.eror_date(lang));
				} else {
					compare = finicio.comparedate(n);
					if (compare == 1) {
						result = true;
					} else if (compare == -1) {
						result = false;
						funciones.print(language.error_before_date(lang));
					} else if (compare == 0) {
						result = false;
						funciones.print(language.error_same_date(lang));
					}
				}
			}

		} while (result == false);
		return n;
	}

	public static int ask_valoration(int lang) {
		Language_utils language = new Language_utils(lang);
		int valoration = 0;
		do {
			valoration = funciones.valida_Int(language.message_ask_valoration(lang),
					language.title_ask_valoration(lang));

			if ((valoration < 0) || (valoration > 10)) {
				funciones.print(language.error_valoration(lang));
			}

		} while ((valoration < 0) || (valoration > 10));
		return valoration;
	}

	public static String ask_primary_key(int lang) {
		Language_utils language = new Language_utils(lang);
		String pk = "";
		boolean interruptor = true;
		do {
			pk = funciones.validaString(language.message_primary_key(lang), language.title_primary_key(lang));
			if (singleton.consumptions.isEmpty()) {
				interruptor = false;
				singleton.consumptions.add(pk);
			} else {
				for (int j = 0; j < singleton.consumptions.size(); j++) {
					if (pk.equals(singleton.consumptions.get(j))) {
						interruptor = true;
						funciones.print(language.primary_key_error(lang));
						break;
					} else {
						interruptor = false;

					}
				}
				if (interruptor == false) {
					funciones.print(language.new_primary_key(lang));
					singleton.consumptions.add(pk);
				}
			}
		} while (interruptor == true);
		return pk;
	}

	public static fecha promotion_date(fecha finicio, fecha fretirada) {
		boolean result = false;
		String fdate = "";
		fecha n = null;
		int compare = 0;
		do {
			fdate = funciones.validaString("Introduzca la fecha de promoci�n", "Promoci�n");
			result = validate.validate_date(fdate);
			if (result == false) {
				funciones.print("Error, fecha no valida");
			} else {
				result = true;
				n = new fecha(fdate);
				result = n.validate_date();
				if (result == false) {
					funciones.print("Error, fecha no v�lida");
				} else {
					compare = finicio.comparedate(n);
					if (compare == 1) {
						result = true;
					} else if (compare == -1) {
						result = false;
						funciones.print("No puedes poner en promoci�n un producto antes de introducirlo");
					} else if (compare == 0) {
						result = true;
					}
					compare = fretirada.comparedate(n);
					if (compare == 1) {
						result = false;
						funciones.print("No puedes poner en promoci�n un producto despu�s de retirarlo");
					} else if (compare == -1) {
						result = true;
					} else if (compare == 0) {
						result = true;
					}
				}
			}

		} while (result == false);
		return n;
	}
}
