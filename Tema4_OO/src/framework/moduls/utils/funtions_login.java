package framework.moduls.utils;

import java.util.ArrayList;

import framework.moduls.classes.singleton;
import framework.moduls.classes.User.User;
import framework.utils.funciones;
import framework.utils.validate;

public class funtions_login {

	public static void login() {
		singleton.user = new ArrayList<User>();
		singleton.user_complete = new ArrayList<User>();
		User u1 = null;
		int op = -1, position = -1;
		boolean result = false;
		String name = "", pasword = "";
		String[] options = { "Crear usuario", "Iniciar sesi�n" };

		do {
			op = funciones.menu_botones(options, "Inicia sesi�n, si no tienes una cuenta puedes registrarte", "Log in");
			if (op == 0) {
				do {
					name = funciones.validaString("Escribe tu nick", "Nickname");
					if (singleton.user.isEmpty()) {
						result = validate.validate_name(name);
						if (result == true) {
							u1 = new User(name);
							singleton.user.add(u1);
						} else {
							funciones.print("El nombre debe empezar por may�suculas y solo caracteres alfab�ticos");
						}
					} else {
						result = validate.validate_name(name);
						if (result == true) {
							u1 = new User(name);
							position = funtions_find.find_user(u1);
							if (position == -1) {
								singleton.user.add(u1);
							} else {
								result = false;
								funciones.print("Ya hay un usuario registrado con ese nombre");
							}
						} else {
							funciones.print("El nombre debe empezar por may�suculas y solo car�cteres alfab�ticos");
						}
					}
				} while (result == false);
				do {
					pasword = funciones.validaString("Escribe tu contrase�a", "Pasword");
					u1 = new User(name, pasword);
					result = funtions_find.equal_pasword(u1);
					if (result == true) {
						singleton.user_complete.add(u1);
					} else {
						result = false;
						funciones.print("La contrase�a y el nombre no pueden ser iguales");
					}
				} while (result == false);
			} else if (op == 1) {
				if (singleton.user_complete.isEmpty()) {
					funciones.print("No hay usuarios registrados");
					result = false;
					op = 0;
				} else {
					do {
						name = funciones.validaString("Escribe tu nick", "Nickname");
						u1 = new User(name);
						position = funtions_find.find_user(u1);
						if (position != -1) {
							pasword = funciones.validaString("Escriba la contrase�a", "Pasword");
							u1 = new User(name, pasword);
							position = funtions_find.find_pasword(u1);
							if (position != -1) {
								result = true;
							} else {
								result = false;
								funciones.print("Contrase�a incorrecta");
							}
						} else {
							funciones.print("Usuario incorrecto");
							result = false;
						}

					} while (result == false);
				}
			}
		} while (op == 0);
	}

}
