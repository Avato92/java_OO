package framework.clases;

public class Language {
	public static Language instance;
	private int lang;

	public static Language getInstance() {
		if (instance == null) {
			instance = new Language(0);

		}
		return instance;
	}

	public Language(int lang) {
		this.lang = lang;
	}

	public int getLang() {
		return this.lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public String messageWorkDummies(int i) {
		String dummies = "";
		switch (i) {
		case 0:
			dummies = "�Quieres trabajar con dummies?";
			break;
		case 1:
			dummies = "Vols treballar amb dummies?";
			break;
		case 2:
			dummies = "Do you want work with dummies?";
			break;
		default:
			dummies = "�Quieres trabajar con dummies?";
			break;
		}

		return dummies;
	}

	public String messageChoiceconsumption(int i) {
		String consumption = "";
		switch (i) {
		case 0:
			consumption = "�Que tipo de consumici�n quieres?";
			break;
		case 1:
			consumption = "Que tipus de consumici� vols?";
			break;
		case 2:
			consumption = "What kind of consumption do you want?";
			break;
		default:
			consumption = "�Qu� tipo de consumici�n quieres?";
			break;
		}
		return consumption;
	}

	public String titleChoiceconsumption(int i) {
		String title = "";
		switch (i) {
		case 0:
			title = "Consumiciones";
			break;
		case 1:
			title = "Consumicions";
			break;
		case 2:
			title = "Consumption";
			break;
		default:
			title = "Consumici�n";
			break;
		}
		return title;
	}

	public String messageCrud(int i) {
		String crud = "";
		switch (i) {
		case 0:
			crud = "�Qu� quieres hacer?";
			break;
		case 1:
			crud = "Qu� vols fer?";
			break;
		case 2:
			crud = "What do you want to do?";
			break;
		default:
			crud = "�Qu� quieres hacer?";
			break;
		}
		return crud;
	}

	public String[] operationsConsumption(int i) {
		String consumption[] = { "", "", "", "" };
		switch (i) {
		case 0:
			consumption[0] = "Comida";
			consumption[1] = "Bebida fria";
			consumption[2] = "Bebida caliente";
			consumption[3] = "Salir";
			break;
		case 1:
			consumption[0] = "Menjar";
			consumption[1] = "Beguda freda";
			consumption[2] = "Beguda calenta";
			consumption[3] = "Eixir";
			break;
		case 2:
			consumption[0] = "Food";
			consumption[1] = "Cold drink";
			consumption[2] = "Hot drink";
			consumption[3] = "Exit";
			break;
		default:
			consumption[0] = "Comida";
			consumption[1] = "Bebida fria";
			consumption[2] = "Bebida caliente";
			consumption[3] = "Salir";
			break;
		}
		return consumption;
	}

	public String[] operationsCRUD(int i) {
		String crud[] = { "", "", "", "", "", "", "" };
		switch (i) {
		case 0:
			crud[0] = "Crear";
			crud[1] = "Leer";
			crud[2] = "Actualizar";
			crud[3] = "Eliminar";
			crud[4] = "Ordenar";
			crud[5] = "Idioma";
			crud[6] = "Atr�s";
			break;
		case 1:
			crud[0] = "Crear";
			crud[1] = "Llegir";
			crud[2] = "Actualitzar";
			crud[3] = "Eliminar";
			crud[4] = "Ordenar";
			crud[5] = "Idioma";
			crud[6] = "Arrere";
			break;
		case 2:
			crud[0] = "Create";
			crud[1] = "Read";
			crud[2] = "Update";
			crud[3] = "Delete";
			crud[4] = "Order";
			crud[5] = "Language";
			crud[6] = "Back";
			break;
		default:
			crud[0] = "Crear";
			crud[1] = "Leer";
			crud[2] = "Actualizar";
			crud[3] = "Eliminar";
			crud[4] = "Ordenar";
			crud[5] = "Idioma";
			crud[6] = "Atr�s";
			break;

		}
		return crud;
	}

	public String[] Y_N(int i) {
		String[] y_n = { "", "" };
		switch (i) {
		case 0:
			y_n[0] = "Si";
			y_n[1] = "No";
			break;
		case 1:
			y_n[0] = "Si";
			y_n[1] = "No";
			break;
		case 2:
			y_n[0] = "Yes";
			y_n[1] = "No";
			break;
		default:
			y_n[0] = "Si";
			y_n[1] = "No";
			break;
		}
		return y_n;
	}

	public String bye(int i) {
		String bye = "";
		switch (i) {
		case 0:
			bye = "Gracias por usar la aplicaci�n!";
			break;
		case 1:
			bye = "Gracies per utilitzar l'aplicaci�!";
			break;
		case 2:
			bye = "Thanks for using the application!";
			break;
		default:
			bye = "Gracias por usar la aplicaci�n!";
		}
		return bye;
	}
}
