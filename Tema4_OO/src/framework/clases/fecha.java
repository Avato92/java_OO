package framework.clases;

import java.util.Calendar;

import framework.utils.funciones;

public class fecha {
	private int day;
	private int month;
	private int year;
	private String date;

	public fecha(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
		this.date = get_date(this.day, this.month, this.year);
	}

	public fecha(String date) {
		this.date = date;
	}

	public int getDay() {
		return this.day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String get_date(int day, int month, int year) {
		String date = "";
		date = day + "/" + month + "/" + year;
		return date;
	}

	public String toString() {
		String date = "";
		date = this.day + "/" + this.month + "/" + this.year;
		return date;

	}

	public boolean validate_date() {
		boolean correct = false;
		String[] arraydate = null;

		arraydate = this.date.split("/");

		day = Integer.parseInt(arraydate[0]);
		month = Integer.parseInt(arraydate[1]);
		year = Integer.parseInt(arraydate[2]);

		if ((year > 2010) && (year < 2030)) {
			if ((month >= 1) && (month <= 12)) {
				switch (month) {
				case 1: // January
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 2: // February

					if ((((year % 100 == 0) && (year % 400 == 0)) || ((year % 100 != 0) && (year % 4 == 0)))
							&& (day > 0) && (day <= 29))
						correct = true;
					else if ((day > 0) && (day <= 28))
						correct = true;
					break;
				case 3: // March
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 4: // April
					if ((day > 0) && (day <= 30))
						correct = true;
					break;
				case 5: // May
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 6: // June
					if ((day > 0) && (day <= 30))
						correct = true;
					break;
				case 7: // July
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 8: // August
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 9: // September
					if ((day > 0) && (day <= 30))
						correct = true;
					break;
				case 10: // October
					if ((day > 0) && (day <= 31))
						correct = true;
					break;
				case 11: // November
					if ((day > 0) && (day <= 30))
						correct = true;
					break;
				case 12: // December

					if ((day > 0) && (day <= 31))
						correct = true;
					break;

				default:
					correct = false;
				}
			} else
				correct = false;
		} else
			correct = false;
		return correct;
	}

	public Calendar StringtoCalendar() {
		Calendar cal = Calendar.getInstance();
		try {
			cal.set(this.getYear(), this.getMonth(), this.getDay());
		} catch (Exception e) {
			funciones.print("No has introducido una fecha valida");
		}
		return cal;
	}

	public int comparedate(fecha n) {
		Calendar date1 = null;
		Calendar date2 = null;

		date2 = n.StringtoCalendar();
		date1 = this.StringtoCalendar();
		if (date1.before(date2))
			return 1;
		else if (date1.after(date2))
			return -1;
		else
			return 0;
	}

}
